# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git init
git remote add origin https://zklara@bitbucket.org/zklara/stroboskop.git
git pull origin master
```

Naloga 6.2.3:
https://bitbucket.org/zklara/stroboskop/commits/5d3e605b638e167a09c4b9a5ca036863f9751c74

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/zklara/stroboskop/commits/bbbdd1bf43c35b481b41db34ebfcee75648ed70e

Naloga 6.3.2:
https://bitbucket.org/zklara/stroboskop/commits/bf5fee29a655a3d53d9bc26005d8cd9c358f7f2d

Naloga 6.3.3:
https://bitbucket.org/zklara/stroboskop/commits/cc133ea0edae6343559481d357843a3adb83f42b
Naloga 6.3.4:
https://bitbucket.org/zklara/stroboskop/commits/8bd0a89e3a8d36ca474b96070db0aa33be528c92

Naloga 6.3.5:

```
git checkout master
git merge izgled --no-ff
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/zklara/stroboskop/commits/77e4605b7296539f986a7902fe701885d819d232

Naloga 6.4.2:
https://bitbucket.org/zklara/stroboskop/commits/e91fee6455ca152b09a42ab72c1a30e5e595d2e5

Naloga 6.4.3:
https://bitbucket.org/zklara/stroboskop/commits/032c28dd1c0a70f615b8aa844f27fe2d8721150a

Naloga 6.4.4:
https://bitbucket.org/zklara/stroboskop/commits/4eb06c5ef6dd8b5649a83113b06639a667f6ce8e